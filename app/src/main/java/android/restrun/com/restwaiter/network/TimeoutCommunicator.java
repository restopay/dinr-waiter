package android.restrun.com.restwaiter.network;

import android.content.Context;
import android.restrun.com.restwaiter.BuildConfig;
import android.restrun.com.restwaiter.R;
import android.restrun.com.restwaiter.model.entities.Bill;
import android.restrun.com.restwaiter.model.entities.Invoice;
import android.restrun.com.restwaiter.model.entities.Login;
import android.restrun.com.restwaiter.model.entities.Order;
import android.restrun.com.restwaiter.model.entities.Restaurant;
import android.restrun.com.restwaiter.model.entities.Session;
import android.restrun.com.restwaiter.model.entities.User;
import android.restrun.com.restwaiter.network.exception.NoConnectivityException;
import android.restrun.com.restwaiter.utils.NetworkUtils;

import com.squareup.okhttp.OkHttpClient;

import java.io.IOException;
import java.io.InputStream;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManagerFactory;

import retrofit.RestAdapter;
import retrofit.client.Client;
import retrofit.client.OkClient;
import retrofit.client.Request;
import retrofit.client.Response;
import rx.Observable;

public class TimeoutCommunicator extends ServerCommunicator
{
    public TimeoutCommunicator(String endpointUrl, Context ctx)
            throws NoSuchAlgorithmException, CertificateException, KeyManagementException, KeyStoreException, IOException {
        super(endpointUrl, ctx);
    }

    protected OkHttpClient initClient()
            throws CertificateException, IOException, KeyStoreException, NoSuchAlgorithmException, KeyManagementException {
        Certificate ca = generateCertificate();
        KeyStore ks = createKeyStore(ca);
        TrustManagerFactory tmf = getTrustManager(ks);
        OkHttpClient client = createClient(tmf);
        client.setReadTimeout(60, TimeUnit.SECONDS);
        client.setConnectTimeout(60, TimeUnit.SECONDS);
        return createClient(tmf);
    }
}