package android.restrun.com.restwaiter.model.helpers;

import java.text.SimpleDateFormat;
import java.util.Date;

public class SimpleDate
{
    public String getDate()
    {
        return new SimpleDateFormat("y-MM-dd").format(new Date());
    }

    public String getTime()
    {
        return new SimpleDateFormat("HH:mm").format(new Date());
    }

    public String getDateTime()
    {
        return getDate() + " " + getTime();
    }
}
