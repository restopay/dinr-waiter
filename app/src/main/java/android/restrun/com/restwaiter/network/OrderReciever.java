package android.restrun.com.restwaiter.network;

import android.annotation.SuppressLint;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.restrun.com.restwaiter.App;
import android.restrun.com.restwaiter.R;
import android.restrun.com.restwaiter.domain.AuthDomain;
import android.restrun.com.restwaiter.helpers.CurrentRestaurant;
import android.restrun.com.restwaiter.model.entities.Order;
import android.restrun.com.restwaiter.model.entities.Restaurant;
import android.restrun.com.restwaiter.repository.OrderRepository;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.j256.ormlite.dao.RuntimeExceptionDao;
import com.j256.ormlite.stmt.QueryBuilder;

import java.io.UnsupportedEncodingException;
import java.sql.SQLException;
import java.util.List;
import java.util.Objects;

import rx.Subscriber;

import static android.restrun.com.restwaiter.Constant.BROADCAST_ACTION;
import static android.restrun.com.restwaiter.Constant.BROADCAST_ACTION_ORDER_NEW;


public class OrderReciever extends BroadcastReceiver
{
    final String LOG_TAG = "myLogs";
    private OrderRepository orderRepo = new OrderRepository();
    private RuntimeExceptionDao<Order, Integer> dao = App.inst().getDatabase().getOrderDao();

    @SuppressLint("NewApi")
    @Override
    public void onReceive(Context context, Intent intent)
    {
        if (Objects.equals(intent.getAction(), BROADCAST_ACTION)) {
//            QueryBuilder<Order, Integer> builder = dao.queryBuilder();
//            builder.limit(Long.valueOf(1));
//            builder.orderBy("ttime", false);
            //try {
                //List<Order> list = dao.query(builder.prepare());

                Restaurant restaurant = App.inst().restaurant().get();
                if (restaurant != null) {
                    orderRepo.getTodaysWaitOrders(new Subscriber<Order.Response>() {
                        @Override
                        public void onCompleted() {}
                        @Override
                        public void onError(Throwable e)
                        {
                            Log.e(LOG_TAG, e.getLocalizedMessage());
                        }
                        @Override
                        public void onNext(Order.Response response)
                        {
                            if (response != null) {
                                List<Order> orders = response.getOrders();

//                                for (Order order : orders) {
//                                    dao.create(order);
//                                }

                                if (orders.size() > 0) {
                                    try {
                                        context.sendBroadcast(new Intent(BROADCAST_ACTION_ORDER_NEW)
                                                .putExtra("order", orders.get(0)));
                                        sendNotification(context, new String("Поступил заказ".getBytes("Windows-1251"), "UTF-8"), intent);
                                    } catch (UnsupportedEncodingException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                        }
                    }, restaurant.getId());
                }

//            } catch (SQLException e) {
//                e.printStackTrace();
//            }
        }
    }

    private void sendNotification(Context ctx, String msg, Intent intent)
    {
        NotificationManager mNotificationManager = (NotificationManager)
                ctx.getSystemService(Context.NOTIFICATION_SERVICE);

        PendingIntent contentIntent = PendingIntent.getActivity(ctx, 0,
                intent, 0);

        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(ctx)
                        .setSmallIcon(R.drawable.launcher)
                        .setContentTitle("Rest.run")
                        .setStyle(new NotificationCompat.BigTextStyle()
                                .bigText(msg))
                        .setContentText(msg);

        try {
            Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            Ringtone r = RingtoneManager.getRingtone(ctx, notification);
            r.play();
        } catch (Exception e) {
            e.printStackTrace();
        }

        mBuilder.setContentIntent(contentIntent);
        mNotificationManager.notify(1, mBuilder.build());
    }
}
