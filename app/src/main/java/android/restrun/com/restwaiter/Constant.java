package android.restrun.com.restwaiter;

public class Constant
{
    public static final String SERVER_ENDPOINT = "https://apirestrun.northeurope.cloudapp.azure.com/api/core/";
    public final static String BROADCAST_ACTION = "android.restrun.com.restwaiter.order.broadcast";
    public final static String BROADCAST_ACTION_ORDER_NEW = "android.restrun.com.restwaiter.order.new";
}
