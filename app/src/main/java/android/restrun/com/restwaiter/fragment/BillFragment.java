package android.restrun.com.restwaiter.fragment;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.restrun.com.restwaiter.R;
import android.restrun.com.restwaiter.helpers.SimpleLogger;
import android.restrun.com.restwaiter.model.entities.Bill;
import android.restrun.com.restwaiter.model.entities.Invoice;
import android.restrun.com.restwaiter.model.entities.Order;
import android.restrun.com.restwaiter.repository.ClientRepository;
import android.restrun.com.restwaiter.repository.OrderRepository;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;


import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import rx.Subscriber;

public class BillFragment extends Fragment implements View.OnClickListener
{
    private ListView mLvBill;
    private BillAdapter mAdapter;
    private List<Bill> mBill = new ArrayList<>();
    private OrderRepository orderRepo = new OrderRepository();
    private Bundle bundle;
    private Button btnApprove;
    private Button btnClose;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.content_bill, container, false);
        this.setAdapter(v);
        this.bindEvents(v);
        this.fetchData();
        return v;
    }

    @SuppressLint("NewApi")
    public void bindEvents(View v)
    {
        bundle = getArguments();
        if (bundle != null) {
            btnApprove = (Button) v.findViewById(R.id.btn_approve);
            btnClose = (Button) v.findViewById(R.id.btn_close);
            btnApprove.setVisibility(View.INVISIBLE);
            btnClose.setVisibility(View.INVISIBLE);

            if (Objects.equals(bundle.getString(Invoice.COLUMN_STATUS), Invoice.STATUS_WAIT)) {
                btnApprove.setVisibility(View.VISIBLE);
            }
            if (Objects.equals(bundle.getString(Invoice.COLUMN_STATUS), Invoice.STATUS_APPROVED)) {
                btnClose.setVisibility(View.VISIBLE);
            }

            btnApprove.setOnClickListener(this);
            btnClose.setOnClickListener(this);
        }
    }

    public void setAdapter(View v)
    {
        mLvBill = (ListView) v.findViewById(R.id.lv_bill);
        mAdapter = new BillAdapter(getActivity(), mBill);
        mLvBill.setAdapter(mAdapter);
    }


    public void fetchData()
    {
        this.fetchBill();
    }

    private void fetchBill()
    {
        if (bundle == null) {
            SimpleLogger.fire(getActivity(), "Error when getting bill!", "Bundle is null!");
            return;
        }
        orderRepo.getBillByInvoice(new Subscriber<Bill.Response>() {
            @Override
            public void onCompleted() {}
            @Override
            public void onError(Throwable e)
            {
                SimpleLogger.log(getActivity(), e.getLocalizedMessage());
            }
            @Override
            public void onNext(Bill.Response response)
            {
                if (response != null) {
                    List<Error> errors = response.getErrors();
                    List<Bill> bill = response.getAll();

                    if (errors != null && !errors.isEmpty()) {
                        String message = errors.get(0).getMessage();
                        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                    } else if (bill != null) {
                        mBill.clear();
                        mBill.addAll(bill);
                        mAdapter.notifyDataSetChanged();
                    }
                } else {
                    SimpleLogger.fire(getActivity(), "Invoice is not found", "Invoice.Response response is empty!");
                }
            }
        }, bundle.getLong(Invoice.COLUMN_ID));
    }

    /**
     * @TODO toasts
     * @param v
     */
    @Override
    public void onClick(View v)
    {
        switch (v.getId()) {
            case R.id.btn_approve:
                orderRepo.approveInvoice(new Subscriber<Invoice.Response>() {
                    @Override
                    public void onCompleted() {}
                    @Override
                    public void onError(Throwable e)
                    {
                        SimpleLogger.log(getActivity(), e.getLocalizedMessage());
                    }
                    @Override
                    public void onNext(Invoice.Response response)
                    {
                        if (response != null) {
                            Invoice invoice = response.getInvoice();
                            if (!invoice.isApproved()) {
                                SimpleLogger.log(getActivity(), "Error of approve bill!");
                            } else {
                                btnApprove.setVisibility(View.INVISIBLE);
                                btnClose.setVisibility(View.VISIBLE);
                                SimpleLogger.success(getActivity(), "Invoice has approved!");
                            }
                        } else {
                            SimpleLogger.fire(getActivity(), "Invoice is not found", "Invoice.Response response is empty!");
                        }
                    }
                }, bundle.getLong(Invoice.COLUMN_ID));
                break;
            case R.id.btn_close:
                orderRepo.closeInvoice(new Subscriber<Invoice.Response>() {
                    @Override
                    public void onCompleted() {}
                    @Override
                    public void onError(Throwable e)
                    {
                        SimpleLogger.log(getActivity(), e.getLocalizedMessage());
                    }
                    @Override
                    public void onNext(Invoice.Response response)
                    {
                        if (response != null) {
                            Invoice invoice = response.getInvoice();
                            if (!invoice.isPaid()) {
                                SimpleLogger.log(getActivity(), "Error of paying bill!");
                            } else {
                                btnClose.setVisibility(View.INVISIBLE);
                                SimpleLogger.success(getActivity(), "Invoice has paid!");
                            }
                        } else {
                            SimpleLogger.fire(getActivity(), "Invoice is not found", "Invoice.Response response is empty!");
                        }
                    }
                }, bundle.getLong(Invoice.COLUMN_ID));
                break;
        }
    }

    private class BillAdapter extends ArrayAdapter<Bill> implements View.OnClickListener {

        public BillAdapter(Context context, List<Bill> dishs) {
            super(context, R.layout.row_bill, dishs);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = LayoutInflater.from(getContext()).inflate(R.layout.row_bill, parent, false);
                convertView.setTag(new ViewHolder(convertView));
            }

            ViewHolder holder = (ViewHolder) convertView.getTag();
            Bill bill = getItem(position);

            if (bill != null) {
                holder.txtName.setText(bill.getTitle());
                holder.txtDishQua.setText(String.valueOf(bill.getQuantity()));
            }
            if (Objects.equals(bundle.getString(Invoice.COLUMN_STATUS), Invoice.STATUS_PAID)) {
                holder.btnDelete.setVisibility(View.INVISIBLE);
            }

            if (holder.btnDelete != null) {
                assert bill != null;
                holder.btnDelete.setTag(R.id.btn_delete, bill.getId());
                holder.btnDelete.setOnClickListener(this);
            }
            return convertView;
        }

        @Override
        public void onClick(View v)
        {
            switch (v.getId()) {
                case R.id.btn_delete:
                    orderRepo.deleteBill(new Subscriber<Bill.Response>() {
                        @Override
                        public void onCompleted() {

                        }

                        @Override
                        public void onError(Throwable e) {

                        }

                        @Override
                        public void onNext(Bill.Response response) {

                        }
                    }, Integer.parseInt(v.getTag(R.id.btn_delete).toString()));
                    break;
            }
        }

        private class ViewHolder
        {
            private final TextView txtName;
            private final TextView txtDishQua;
            private final Button btnDelete;

            public ViewHolder(View view) {
                txtName = ((TextView) view.findViewById(R.id.txt_name));
                txtDishQua = ((TextView) view.findViewById(R.id.txt_dish_qua));
                btnDelete = ((Button) view.findViewById(R.id.btn_delete));
            }
        }
    }
}
