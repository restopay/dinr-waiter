package android.restrun.com.restwaiter.model;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.restrun.com.restwaiter.model.entities.Client;
import android.restrun.com.restwaiter.model.entities.Order;
import android.restrun.com.restwaiter.model.entities.Restaurant;
import android.restrun.com.restwaiter.model.entities.User;
import android.util.Log;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.RuntimeExceptionDao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;

public class DatabaseHelper extends OrmLiteSqliteOpenHelper
{
    private static final String LOG_TAG = DatabaseHelper.class.getSimpleName();
    public static final String DATABASE_NAME = "db.sqlite";
    public static final int DATABASE_VERSION = 1;

    private RuntimeExceptionDao<Restaurant, Integer> restaurantDao;
    private RuntimeExceptionDao<Client, Integer> clientDao;
    private RuntimeExceptionDao<User, Integer> userDao;
    private RuntimeExceptionDao<Order, Integer> orderDao;

    public DatabaseHelper(Context context)
    {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db, ConnectionSource connectionSource)
    {
        Log.d(LOG_TAG, "onCreate called");
        createTables();
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, ConnectionSource connectionSource, int oldVersion, int newVersion)
    {
        Log.d(LOG_TAG, "onUpdate called");
        dropTables();
        createTables();
    }

    private void createTables()
    {
        try {
            Log.i(LOG_TAG, "Create tables");
            TableUtils.createTable(connectionSource, Restaurant.class);
            TableUtils.createTable(connectionSource, Client.class);
            TableUtils.createTable(connectionSource, User.class);
            TableUtils.createTable(connectionSource, Order.class);
        } catch (SQLException e) {
            Log.e(LOG_TAG, "Can't create database", e);
            throw new RuntimeException(e);
        }
    }

    private void dropTables()
    {
        try {
            Log.d(LOG_TAG, "Drop tables");
            TableUtils.dropTable(connectionSource, Restaurant.class, false);
            TableUtils.dropTable(connectionSource, Client.class, false);
            TableUtils.dropTable(connectionSource, User.class, false);
            TableUtils.dropTable(connectionSource, Order.class, false);
        } catch (SQLException e) {
            Log.e(LOG_TAG, "Can't drop tables", e);
            throw new RuntimeException(e);
        }
    }

    public void clearTable(Class className)
    {
        try {
            TableUtils.clearTable(connectionSource, className);
        } catch (SQLException e) {
            Log.e(LOG_TAG, "Can't drop tables", e);
            throw new RuntimeException(e);
        }
    }

    public void clearAllTables()
    {
        try {
            TableUtils.clearTable(connectionSource, Restaurant.class);
        } catch (SQLException e) {
            Log.e(LOG_TAG, "Can't drop tables", e);
            throw new RuntimeException(e);
        }
    }

    public RuntimeExceptionDao<Restaurant, Integer> getRestaurantDao()
    {
        if (restaurantDao == null) {
            restaurantDao = getRuntimeExceptionDao(Restaurant.class);
        }
        return restaurantDao;
    }

    public RuntimeExceptionDao<Client, Integer> getClientDao()
    {
        if (clientDao == null) {
            clientDao = getRuntimeExceptionDao(Client.class);
        }
        return clientDao;
    }

    public RuntimeExceptionDao<User, Integer> getUserDao()
    {
        if (userDao == null) {
            userDao = getRuntimeExceptionDao(User.class);
        }
        return userDao;
    }

    public RuntimeExceptionDao<Order, Integer> getOrderDao()
    {
        if (orderDao == null) {
            orderDao = getRuntimeExceptionDao(Order.class);
        }
        return orderDao;
    }

    @Override
    public void close()
    {
        super.close();
        restaurantDao = null;
        clientDao = null;
        userDao = null;
        orderDao = null;
    }
}