package android.restrun.com.restwaiter.repository;

import android.restrun.com.restwaiter.App;
import android.restrun.com.restwaiter.model.entities.Client;
import android.restrun.com.restwaiter.model.entities.Order;
import android.restrun.com.restwaiter.model.entities.Restaurant;
import android.restrun.com.restwaiter.network.ServerCommunicator;

import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class ClientRepository
{
    private Subscription subscribe;
    private ServerCommunicator communicator;

    public ClientRepository()
    {
        communicator = App.inst().getCommunicator();
    }

    public void getClient(Subscriber<Client.Response> cb, long id)
    {
        subscribe = Observable.just(1)
                .subscribeOn(Schedulers.newThread())
                .flatMap(empty -> communicator.getClient(id))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(cb);
    }

    public void getRestaurantByClient(Subscriber<Restaurant.Response> cb, long id)
    {
        subscribe = Observable.just(1)
                .subscribeOn(Schedulers.newThread())
                .flatMap(empty -> communicator.getRestaurantByClient(id))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(cb);
    }
}
