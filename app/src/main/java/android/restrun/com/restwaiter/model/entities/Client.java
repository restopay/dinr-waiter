package android.restrun.com.restwaiter.model.entities;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.List;

@DatabaseTable(tableName = Client.TABLE_NAME)
public class Client
{
    public static final String TABLE_NAME = "client";
    public static final String COLUMN_ID = "id";
    public static final String COLUMN_BALANCE = "balance";
    public static final String COLUMN_HEAD = "head";
    public static final String COLUMN_PHONE = "phone";
    public static final String COLUMN_EMAIL = "email";
    public static final String COLUMN_DATE_CREATE = "date_create";
    public static final String COLUMN_DATE_UPDATE = "date_update";
    public static final String COLUMN_TITLE = "title";
    public static final String COLUMN_TYPE = "type";
    public static final String COLUMN_KPP = "kpp";
    public static final String COLUMN_INN = "inn";


    @DatabaseField(columnName = COLUMN_ID, generatedId = true)
    private long id;

    @DatabaseField(columnName = COLUMN_BALANCE)
    private double balance;

    @DatabaseField(columnName = COLUMN_PHONE)
    private String phone;

    @DatabaseField(columnName = COLUMN_HEAD)
    private String head;

    @DatabaseField(columnName = COLUMN_EMAIL)
    private String email;

    @DatabaseField(columnName = COLUMN_DATE_CREATE)
    private String date_create;

    @DatabaseField(columnName = COLUMN_DATE_UPDATE)
    private String date_update;

    @DatabaseField(columnName = COLUMN_TITLE)
    private String title;

    @DatabaseField(columnName = COLUMN_TYPE)
    private String type;

    @DatabaseField(columnName = COLUMN_KPP)
    private String kpp;

    @DatabaseField(columnName = COLUMN_INN)
    private String inn;

    public Client() {}

    public long getId()
    {
        return this.id;
    }

    public String getType()
    {
        return this.type;
    }

    public String getTitle()
    {
        return this.title;
    }

    public class Response
    {
        private List<Client> success;
        private List<Error> errors;

        public List<Error> getErrors()
        {
            return errors;
        }

        public List<Client> getClients()
        {
            return success;
        }
        public Client getClient()
        {
            return success.get(0);
        }
    }
}