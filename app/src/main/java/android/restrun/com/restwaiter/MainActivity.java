package android.restrun.com.restwaiter;

import android.content.Intent;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.restrun.com.restwaiter.activity.AppActivity;
import android.restrun.com.restwaiter.activity.LoginActivity;
import android.restrun.com.restwaiter.domain.AuthDomain;
import android.restrun.com.restwaiter.model.helpers.SystemRegistry;
import android.restrun.com.restwaiter.network.FoneService;
import android.support.v7.app.AppCompatActivity;
import android.text.format.Formatter;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        WifiManager wm = (WifiManager) getSystemService(WIFI_SERVICE);
        String ip = Formatter.formatIpAddress(wm.getConnectionInfo().getIpAddress());
        SystemRegistry.addIp(ip);
        this.login();
    }

    public void login()
    {
        if (!new AuthDomain(this).issetUser()) {
            Intent intent = new Intent(this, LoginActivity.class);
            startActivityForResult(intent, 1);
        } else {
            //new TableDomain(this).collectInvoice();
            Intent intent = new Intent(this, AppActivity.class);
            startActivity(intent);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            this.login();
        }
    }
}
