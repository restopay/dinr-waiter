package android.restrun.com.restwaiter.activity;

import android.content.Intent;
import android.os.Bundle;
import android.restrun.com.restwaiter.MainActivity;
import android.support.v7.app.AppCompatActivity;

public class SplashActivity extends AppCompatActivity
{
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }
}
