package android.restrun.com.restwaiter.network;

import android.annotation.SuppressLint;
import android.content.Context;
import android.restrun.com.restwaiter.BuildConfig;
import android.restrun.com.restwaiter.R;
import android.restrun.com.restwaiter.model.entities.Bill;
import android.restrun.com.restwaiter.model.entities.Card;
import android.restrun.com.restwaiter.model.entities.CardMatrix;
import android.restrun.com.restwaiter.model.entities.CouponStorage;
import android.restrun.com.restwaiter.model.entities.Dish;
import android.restrun.com.restwaiter.model.entities.Invoice;
import android.restrun.com.restwaiter.model.entities.Login;
import android.restrun.com.restwaiter.model.entities.Order;
import android.restrun.com.restwaiter.model.entities.Restaurant;
import android.restrun.com.restwaiter.model.entities.Session;
import android.restrun.com.restwaiter.model.entities.User;
import android.restrun.com.restwaiter.network.exception.NoConnectivityException;
import android.restrun.com.restwaiter.utils.NetworkUtils;

import com.squareup.okhttp.OkHttpClient;

import java.io.IOException;
import java.io.InputStream;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.util.List;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManagerFactory;

import retrofit.RestAdapter;
import retrofit.client.Client;
import retrofit.client.OkClient;
import retrofit.client.Request;
import retrofit.client.Response;
import rx.Observable;

public class ServerCommunicator
{

    protected final IWebService mWebService;
    private Context ctx;

    // Создание экземпляра подключения
    public ServerCommunicator(String endpointUrl, Context ctx)
            throws NoSuchAlgorithmException, CertificateException, KeyManagementException, KeyStoreException, IOException {
        this.ctx = ctx;

        OkHttpClient client = this.initClient();

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(endpointUrl)
                .setLogLevel(BuildConfig.DEBUG ? RestAdapter.LogLevel.FULL : RestAdapter.LogLevel.NONE)
                .setErrorHandler(new RetrofitErrorHandler())
                .setClient(new WrappedClient(new OkClient(client)))
                .build();

        mWebService = restAdapter.create(IWebService.class);
    }

    protected OkHttpClient initClient() throws CertificateException, IOException, KeyStoreException, NoSuchAlgorithmException, KeyManagementException {
        Certificate ca = generateCertificate();
        KeyStore ks = createKeyStore(ca);
        TrustManagerFactory tmf = getTrustManager(ks);
        return createClient(tmf);
    }

    @SuppressLint("NewApi")
    protected Certificate generateCertificate() throws CertificateException, IOException
    {
        CertificateFactory cf = CertificateFactory.getInstance("X.509");
        Certificate ca;

        try (InputStream cert = ctx.getResources().openRawResource(R.raw.cert)) {
            ca = cf.generateCertificate(cert);
        }

        return ca;
    }

    protected KeyStore createKeyStore(Certificate ca)
            throws KeyStoreException, CertificateException, NoSuchAlgorithmException, IOException
    {
        String keyStoreType = KeyStore.getDefaultType();
        KeyStore keyStore = KeyStore.getInstance(keyStoreType);
        keyStore.load(null, null);
        keyStore.setCertificateEntry("ca", ca);
        return keyStore;
    }

    protected TrustManagerFactory getTrustManager(KeyStore keyStore)
            throws NoSuchAlgorithmException, KeyStoreException
    {
        String tmfAlgorithm = TrustManagerFactory.getDefaultAlgorithm();
        TrustManagerFactory tmf = TrustManagerFactory.getInstance(tmfAlgorithm);
        tmf.init(keyStore);
        return tmf;
    }

    protected OkHttpClient createClient(TrustManagerFactory tmf)
            throws NoSuchAlgorithmException, KeyManagementException
    {
        OkHttpClient okHttpClient = new OkHttpClient();

        SSLContext sslContext = SSLContext.getInstance("TLS");
        sslContext.init(null, tmf.getTrustManagers(), null);
        okHttpClient.setSslSocketFactory(sslContext.getSocketFactory());
        return okHttpClient;
    }


    protected class WrappedClient implements Client
    {
        WrappedClient(Client wrappedClient)
        {
            this.wrappedClient = wrappedClient;
        }

        Client wrappedClient;

        @Override
        public Response execute(Request request) throws IOException
        {
            if (!NetworkUtils.isNetworkAvailable()) {
                throw new NoConnectivityException("No connectivity");
            }
            return wrappedClient.execute(request);
        }
    }

    public Observable<Restaurant.Response> getRestaurantByClient(long id)
    {
        return mWebService.getRestaurantByClient(id);
    }

    public Observable<Invoice.Response> getInvoiceByOrder(long orderId)
    {
        return mWebService.getInvoiceByOrder(orderId);
    }

    public Observable<Bill.Response> getBillByInvoice(long invoiceId)
    {
        return mWebService.getBillByInvoice(invoiceId);
    }

    public Observable<Login.Response> getLogin(String login, String password)
    {
        return mWebService.getLogin(login, password);
    }

    public Observable<Session.Response> createSession(Session session)
    {
        return mWebService.createSession(session);
    }

    public Observable<User.Response> getUser(long loginId)
    {
        return mWebService.getUser(loginId);
    }


    public Observable<Order.Response> getTodaysOrders(long userId)
    {
        return mWebService.getTodaysOrders(userId);
    }

    public Observable<Order.Response> getTodaysWaitOrders(long restId)
    {
        return mWebService.getTodaysWaitOrders(restId);
    }

    public Observable<Order.Response> getWaitOrders(long restId, long userId,  String stamp)
    {
        return mWebService.getWaitOrders(restId, userId, stamp);
    }

    public Observable<android.restrun.com.restwaiter.model.entities.Client.Response> getClient(long id)
    {
        return mWebService.getClient(id);
    }

    public Observable<Invoice.Response> updateInvoice(Invoice invoice)
    {
        return mWebService.updateInvoice(invoice);
    }

    public Observable<Invoice.Response> approveInvoice(long invoiceId)
    {
        return mWebService.approveInvoice(invoiceId);
    }

    public Observable<Order.Response> closeOrder(long orderId)
    {
        return mWebService.closeOrder(orderId);
    }

    public Observable<Order.Response> dismissOrder(long orderId)
    {
        return mWebService.dismissOrder(orderId);
    }

    public Observable<Order.Response> activateOrder(long orderId, long waiterId)
    {
        return mWebService.activateOrder(orderId, waiterId);
    }

    public Observable<Bill.Response> deleteBill(long id)
    {
        return mWebService.deleteBill(id);
    }

    public Observable<Invoice.Response> payInvoice(long invoiceId)
    {
        return mWebService.payInvoice(invoiceId);
    }
}