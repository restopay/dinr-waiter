package android.restrun.com.restwaiter.helpers;

import android.restrun.com.restwaiter.model.entities.Client;
import android.restrun.com.restwaiter.model.entities.Invoice;

import java.util.Objects;

public class CurrentClient
{
    private static CurrentClient instance;
    private Client client;

    public static CurrentClient getInstance()
    {
        if (instance != null) return instance;
        instance = new CurrentClient();
        return instance;
    }

    private CurrentClient() {}

    public void set(Client client)
    {
        this.client = client;
    }

    public Client get()
    {
        return this.client;
    }

}
