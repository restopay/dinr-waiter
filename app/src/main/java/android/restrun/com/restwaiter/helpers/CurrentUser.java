package android.restrun.com.restwaiter.helpers;

import android.restrun.com.restwaiter.model.entities.Client;
import android.restrun.com.restwaiter.model.entities.User;

public class CurrentUser
{
    private static CurrentUser instance;
    private User user;

    public static CurrentUser getInstance()
    {
        if (instance != null) return instance;
        instance = new CurrentUser();
        return instance;
    }

    private CurrentUser() {}

    public void set(User client)
    {
        this.user = client;
    }

    public User get()
    {
        return this.user;
    }

}
