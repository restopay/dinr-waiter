package android.restrun.com.restwaiter.helpers;


import android.restrun.com.restwaiter.App;
import android.restrun.com.restwaiter.model.entities.Client;

import com.j256.ormlite.dao.RuntimeExceptionDao;

public class ClientInstance
{
    private RuntimeExceptionDao<Client, Integer> dao;

    public ClientInstance()
    {
        dao = App.inst().getDatabase().getClientDao();
    }

    public void set(Client client)
    {
        if (dao.queryForAll().isEmpty()) {
            dao.create(client);
        }
    }

    public Client get()
    {
        return dao.queryForAll().get(0);
    }

}
