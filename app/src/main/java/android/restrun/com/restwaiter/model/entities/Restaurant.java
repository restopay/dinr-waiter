package android.restrun.com.restwaiter.model.entities;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.List;

@DatabaseTable(tableName = Restaurant.TABLE_NAME)
public class Restaurant
{
    public static final String TABLE_NAME = "restaurant";
    public static final String COLUMN_ID = "id";
    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_CHECK = "check";
    public static final String COLUMN_DESCRIPTION = "description";
    public static final String COLUMN_TYPE = "type";
    public static final String COLUMN_ADDRESS = "address";
    public static final String COLUMN_LATITUDE = "latitude";
    public static final String COLUMN_LONGTITUDE = "longtitude";
    public static final String COLUMN_COUPONS = "cnt";


    @DatabaseField(columnName = COLUMN_ID, generatedId = true)
    private int id;

    @DatabaseField(columnName = COLUMN_NAME)
    private String name;

    @DatabaseField(columnName = COLUMN_DESCRIPTION)
    private String description;

    @DatabaseField(columnName = COLUMN_CHECK)
    private String check;

    @DatabaseField(columnName = COLUMN_TYPE)
    private String type;

    @DatabaseField(columnName = COLUMN_ADDRESS)
    private String address;

    @DatabaseField(columnName = COLUMN_LATITUDE)
    private double latitude;

    @DatabaseField(columnName = COLUMN_LONGTITUDE)
    private double longtitude;

    @DatabaseField(columnName = COLUMN_COUPONS)
    private int cnt;


    public Restaurant() {}

    public int getId() {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public String getCheck() {
        return check;
    }

    public String getName() {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public double getLatitude()
    {
        return this.latitude;
    }

    public double getLongtitude()
    {
        return this.longtitude;
    }

    public int getCouponsCnt()
    {
        return this.cnt;
    }

    public String getAddress()
    {
        return this.address;
    }

    public class Response
    {
        private List<Restaurant> success;
        private List<Error> errors;

        public List<Error> getErrors() {
            return errors;
        }

        public List<Restaurant> getRestaurants()
        {
            return success;
        }

        public Restaurant getRestaurant()
        {
            return success.get(0);
        }
    }
}