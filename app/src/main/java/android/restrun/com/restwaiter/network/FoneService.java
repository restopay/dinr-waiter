package android.restrun.com.restwaiter.network;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.IBinder;
import android.restrun.com.restwaiter.Constant;
import android.restrun.com.restwaiter.R;
import android.restrun.com.restwaiter.activity.AppActivity;
import android.restrun.com.restwaiter.helpers.CurrentRestaurant;
import android.restrun.com.restwaiter.model.DatabaseHelper;
import android.restrun.com.restwaiter.model.entities.Order;
import android.restrun.com.restwaiter.repository.OrderRepository;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.j256.ormlite.dao.RuntimeExceptionDao;

import java.io.IOException;
import java.lang.*;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;


public class FoneService extends Service
{
    final String LOG_TAG = "myLogs";
    private Thread thread;
    private OrderRepository orderRepo = new OrderRepository();
    private RuntimeExceptionDao<Order, Integer> dao =
            new DatabaseHelper(this).getOrderDao();
    private TimeoutCommunicator communicator;

    @Nullable
    @Override
    public IBinder onBind(Intent intent)
    {
        Log.d(LOG_TAG, "onBind");
        return null;
    }

    @Override
    public void onCreate()
    {
        super.onCreate();
        Log.d(LOG_TAG, "onCreate");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        startLoop();
        Log.d(LOG_TAG, "onStartCommand");
        return super.onStartCommand(intent, flags, startId);
    }

    private void startLoop()
    {
//        orderRepo.getWaitOrders(new Subscriber<Order.Response>() {
//            @Override
//            public void onCompleted() {}
//            @Override
//            public void onError(Throwable e) {
//                Log.e(LOG_TAG, e.getLocalizedMessage());
//            }
//
//            @Override
//            public void onNext(Order.Response response)
//            {
//                Log.d(LOG_TAG, "onNext");
//
//                sendNotification("Hello!");
//            }
//        }, CurrentRestaurant.getInstance().get().getId(), "2016-11-27 00:00:00");
    }


    private void sendNotification(String msg)
    {
        NotificationManager mNotificationManager = (NotificationManager)
                this.getSystemService(Context.NOTIFICATION_SERVICE);

        PendingIntent contentIntent = PendingIntent.getActivity(this, 0,
                new Intent(this, AppActivity.class), 0);

        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.ic_menu_send)
                        .setContentTitle("DINR")
                        .setSmallIcon(R.drawable.ic_menu_send)
                        .setStyle(new NotificationCompat.BigTextStyle()
                                .bigText(msg))
                        .setContentText(msg);

        try {
            Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            Ringtone r = RingtoneManager.getRingtone(getApplicationContext(), notification);
            r.play();
        } catch (Exception e) {
            e.printStackTrace();
        }

        mBuilder.setContentIntent(contentIntent);
        mNotificationManager.notify(1, mBuilder.build());
    }
}
