package android.restrun.com.restwaiter.helpers;


import android.restrun.com.restwaiter.App;
import android.restrun.com.restwaiter.model.DatabaseHelper;
import android.restrun.com.restwaiter.model.entities.User;

import com.j256.ormlite.dao.RuntimeExceptionDao;

public class UserInstance
{
    private RuntimeExceptionDao<User, Integer> dao;

    public UserInstance()
    {
        dao = App.inst().getDatabase().getUserDao();
    }

    public void set(User client)
    {
        if (dao.queryForAll().isEmpty()) {
            dao.create(client);
        }
    }

    public User get()
    {
        return dao.queryForAll().get(0);
    }

}
