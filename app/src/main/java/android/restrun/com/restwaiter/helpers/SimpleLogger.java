package android.restrun.com.restwaiter.helpers;

import android.app.Activity;
import android.content.Context;
import android.restrun.com.restwaiter.activity.AppActivity;
import android.util.Log;
import android.widget.Toast;

public class SimpleLogger
{
    public static void log(Context context, String message)
    {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
        Log.e("API Error: ", message);
    }

    public static void fire(Context context, String message, String log)
    {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
        Log.e("API Error: ", log);
    }

    public static void success(Context context, String message)
    {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
        Log.i("API Success: ", message);
    }
}
