package android.restrun.com.restwaiter.model.entities;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.List;

@DatabaseTable(tableName = Card.TABLE_NAME)
public class Card
{
    public static final int CARD_TYPE_COUPON = 1;
    public static final int CARD_TYPE_EVENT = 2;

    public static final String TABLE_NAME = "card";
    public static final String COLUMN_ID = "id";
    public static final String COLUMN_RESTAURANT_ID = "restaurant_id";
    public static final String COLUMN_TYPE = "type";
    public static final String COLUMN_DESCRIPTION = "description";
    public static final String COLUMN_SEGMENT_ID = "segment_id";
    public static final String COLUMN_FILE_ID = "file_id";
    public static final String COLUMN_QUANTITY = "quantity";
    public static final String COLUMN_TITLE = "title";


    @DatabaseField(columnName = COLUMN_ID, generatedId = true)
    private long id;

    @DatabaseField(columnName = COLUMN_RESTAURANT_ID)
    private long restaurant_id;

    @DatabaseField(columnName = COLUMN_DESCRIPTION)
    private String description;

    @DatabaseField(columnName = COLUMN_SEGMENT_ID)
    private long segment_id;

    @DatabaseField(columnName = COLUMN_TYPE)
    private int type;

    @DatabaseField(columnName = COLUMN_FILE_ID)
    private long file_id;

    @DatabaseField(columnName = COLUMN_QUANTITY)
    private int quantity;

    @DatabaseField(columnName = COLUMN_TITLE)
    private String title;

    public Card() {}

    public long getId()
    {
        return this.id;
    }

    public String getDescription()
    {
        return this.description;
    }

    public int getType()
    {
        return this.type;
    }

    public boolean isCoupon()
    {
        return this.type == CARD_TYPE_COUPON;
    }

    public int getQuantity()
    {
        return this.quantity;
    }

    public String getTitle()
    {
        return this.title;
    }

    public class Response
    {
        private List<Card> success;
        private List<Error> errors;

        public List<Error> getErrors()
        {
            return errors;
        }

        public List<Card> getCards()
        {
            return success;
        }
    }
}