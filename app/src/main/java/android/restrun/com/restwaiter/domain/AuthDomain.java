package android.restrun.com.restwaiter.domain;

import android.content.Context;
import android.restrun.com.restwaiter.exception.ApiException;
import android.restrun.com.restwaiter.exception.ResponseException;
import android.restrun.com.restwaiter.helpers.CurrentClient;
import android.restrun.com.restwaiter.model.entities.Client;
import android.restrun.com.restwaiter.model.entities.User;
import android.restrun.com.restwaiter.repository.AuthRepository;
import android.restrun.com.restwaiter.model.entities.Session;
import android.restrun.com.restwaiter.model.helpers.LocalStorage;
import android.restrun.com.restwaiter.repository.ClientRepository;
import android.util.Log;

import rx.Subscriber;

public class AuthDomain
{
    private AuthRepository authRepo;
    private Context context;
    private ClientRepository clientRepo = new ClientRepository();


    public AuthDomain(Context ctx)
    {
        this.authRepo = new AuthRepository();
        this.context = ctx;
    }

    public void createSession(Session session)
    {
        authRepo.createSession(new Subscriber<Session.Response>() {
            @Override
            public void onNext(Session.Response response)
            {

            }

            @Override
            public void onCompleted() {
            }

            @Override
            public void onError(Throwable e) {
                Log.e("API Error: ", e.getLocalizedMessage());
            }
        }, session);
    }

    /*private void storeUser(long loginId)
    {
        authRepo.getUser(new Subscriber<User.Response>() {
            @Override
            public void onNext(User.Response response) {
                new LocalStorage(ctx).putLong("user_id", response.getUsers().get(0).getId());

                new RestaurantsFragment();
            }

            @Override
            public void onCompleted() {
            }

            @Override
            public void onError(Throwable e) {
                Log.e("API Error: ", e.getLocalizedMessage());
            }
        }, loginId);
    }
*/
    public long getLocalUserId()
    {
        return new LocalStorage(context).getLong("user_id", 0);
    }

    public boolean issetUser()
    {
        return this.getLocalUserId() != 0;
    }
}

