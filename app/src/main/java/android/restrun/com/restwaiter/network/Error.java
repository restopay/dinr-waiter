package android.restrun.com.restwaiter.network;

public class Error
{
    private String message;
    private int code;

    public Error() {
    }

    public String getMessage()
    {
        return message;
    }

    public void setMessage(String message)
    {
        this.message = message;
    }

    public int getCode()
    {
        return code;
    }

    public void setCode(int code)
    {
        this.code = code;
    }
}
