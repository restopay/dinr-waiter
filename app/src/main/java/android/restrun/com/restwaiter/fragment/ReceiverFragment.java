package android.restrun.com.restwaiter.fragment;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.restrun.com.restwaiter.App;
import android.restrun.com.restwaiter.R;
import android.restrun.com.restwaiter.dialog.NewOrderDialog;
import android.restrun.com.restwaiter.domain.AuthDomain;
import android.restrun.com.restwaiter.helpers.SimpleLogger;
import android.restrun.com.restwaiter.model.entities.Order;
import android.restrun.com.restwaiter.model.entities.Restaurant;
import android.restrun.com.restwaiter.repository.OrderRepository;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import rx.Subscriber;

import static android.restrun.com.restwaiter.Constant.BROADCAST_ACTION_ORDER_NEW;

public class ReceiverFragment extends Fragment implements NewOrderDialog.ConfirmListener
{
    private App app = App.inst();
    final static String ORDER_DIALOG_FRAGMENT = "orderDialog";
    private ListView mLvOrders;
    private OrdersAdapter mAdapter;
    private List<Order> mOrders = new ArrayList<>();
    private OrderRepository orderRepo = new OrderRepository();
    private Restaurant cr;
    public boolean isPaused = false;
    private View v;
    protected FragmentTransaction ft = null;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.content_order, container, false);
        this.setAdapter(v);
        this.fetchData();
        getActivity()
                .getApplicationContext()
                .registerReceiver(broadcastReceiver, new IntentFilter(BROADCAST_ACTION_ORDER_NEW));
        return v;
    }

    @Override
    public void onPause()
    {
        super.onPause();
        this.isPaused = true;
    }

    BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @SuppressLint("NewApi")
        @Override
        public void onReceive(Context context, Intent intent)
        {
            if (Objects.equals(intent.getAction(), BROADCAST_ACTION_ORDER_NEW) && !isPaused) {
                Bundle b = new Bundle();
                b.putSerializable("order", intent.getSerializableExtra("order"));
                NewOrderDialog pf = new NewOrderDialog();

                pf.setArguments(b);
                pf.setTargetFragment(ReceiverFragment.this, 1);
                pf.show(getActivity().getFragmentManager(), ORDER_DIALOG_FRAGMENT);
            }
        }
    };

    public void setAdapter(View v)
    {
        mLvOrders = (ListView) v.findViewById(R.id.lv_orders);
        mAdapter = new OrdersAdapter(getActivity(), mOrders);
        mLvOrders.setAdapter(mAdapter);
        mLvOrders.setOnItemClickListener((adapterView, view, position, l) -> {
            Bundle arguments = new Bundle();
            arguments.putLong(Order.COLUMN_ID, mOrders.get(position).getId());
            arguments.putString(Order.COLUMN_STATUS, mOrders.get(position).getStatus());
            GuestsFragment gf = new GuestsFragment();
            gf.setArguments(arguments);

            ft = getActivity().getFragmentManager().beginTransaction();
            ft.replace(R.id.content_main, gf);
            ft.addToBackStack("order");
            ft.commit();
        });
    }

    public void fetchData()
    {
        this.fetchTodaysOrders(new AuthDomain(getActivity()).getLocalUserId());
    }

    private void fetchTodaysOrders(long userId)
    {
        orderRepo.getTodaysOrders(new Subscriber<Order.Response>() {
            @Override
            public void onCompleted() {}
            @Override
            public void onError(Throwable e)
            {
                SimpleLogger.log(getActivity(), e.getLocalizedMessage());
            }
            @Override
            public void onNext(Order.Response response)
            {
                if (response != null) {
                    List<Error> errors = response.getErrors();
                    List<Order> orders = response.getOrders();

                    if (errors != null && !errors.isEmpty()) {
                        String message = errors.get(0).getMessage();
                        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                    } else if (orders != null) {
                        mOrders.clear();
                        mOrders.addAll(orders);
                        mAdapter.notifyDataSetChanged();
                    }
                } else {
                    SimpleLogger.fire(getActivity(), "Orders is not found", "Order.Response response is empty!");
                }
            }
        }, userId);
    }

    @Override
    public void onConfirmSubmit(int state)
    {
        this.fetchData();
    }

    private class OrdersAdapter extends ArrayAdapter<Order>
    {
        public OrdersAdapter(Context context, List<Order> dishs) {
            super(context, R.layout.row_orders, dishs);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = LayoutInflater.from(getContext()).inflate(R.layout.row_orders, parent, false);
                convertView.setTag(new ViewHolder(convertView));
            }

            ViewHolder holder = (ViewHolder) convertView.getTag();
            Order order = getItem(position);

            if (order != null) {
                holder.txtOrderName.setText(order.getNumber());
                holder.textWaitCnt.setText(String.valueOf(order.getWaitCnt()));
                if (order.getWaitCnt() == 0) {
                    holder.textWaitCnt.setVisibility(View.INVISIBLE);
                }
                if (order.isClosed()) {
                    holder.cardView.setCardBackgroundColor(getResources().getColor(R.color.mains));
                }
            }
            return convertView;
        }

        private class ViewHolder
        {
            private final TextView txtOrderName;
            private final TextView textWaitCnt;
            private final CardView cardView;
//            private final Button btnCheck;
            //private final CheckBox checkDishId;

            public ViewHolder(View view) {
                txtOrderName = ((TextView) view.findViewById(R.id.txt_order_name));
                textWaitCnt = ((TextView) view.findViewById(R.id.txt_wait_cnt));
                cardView = ((CardView) view.findViewById(R.id.card_view));
//                txtDishCost = ((TextView) view.findViewById(R.id.txt_dish_cost));
//                btnCheck = ((Button) view.findViewById(R.id.btn_check));
                //checkDishId = ((CheckBox) view.findViewById(R.id.check_dish));
            }
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        getActivity()
                .getApplicationContext()
                .unregisterReceiver(broadcastReceiver);
    }
}
