package android.restrun.com.restwaiter.exception;

public class ApiException extends Throwable
{
    public ApiException(String message) {
        super(message);
    }
}
