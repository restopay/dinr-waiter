package android.restrun.com.restwaiter.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.restrun.com.restwaiter.App;

public class NetworkUtils
{
    // Проверка наличия подключения к сети
    public static boolean isNetworkAvailable()
    {
        ConnectivityManager cm =
                (ConnectivityManager) App.inst().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }

}
