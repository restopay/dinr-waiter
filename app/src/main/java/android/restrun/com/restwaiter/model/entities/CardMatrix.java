package android.restrun.com.restwaiter.model.entities;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.List;

@DatabaseTable(tableName = CardMatrix.TABLE_NAME)
public class CardMatrix
{
    public static final String TABLE_NAME = "card";
    public static final String COLUMN_TYPE = "type";
    public static final String COLUMN_CNT = "cnt";

    @DatabaseField(columnName = COLUMN_TYPE)
    private int type;

    @DatabaseField(columnName = COLUMN_CNT)
    private int cnt;

    public CardMatrix() {}

    public int getCnt()
    {
        return this.cnt;
    }

    public int getType()
    {
        return this.type;
    }

    public static int getTypeCnt(List<CardMatrix> matrix, int type)
    {
        int cnt = 0;
        for (CardMatrix cm : matrix) {
            if (cm.getType() == type) cnt = cm.getCnt();
        }
        return cnt;
    }

    public class Response
    {
        private List<CardMatrix> success;
        private List<Error> errors;

        public List<Error> getErrors()
        {
            return errors;
        }

        public List<CardMatrix> get()
        {
            return success;
        }
    }
}