package android.restrun.com.restwaiter.model.entities;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.List;

@DatabaseTable(tableName = Dish.TABLE_NAME)
public class Dish {

    public static final String TABLE_NAME = "dish";
    public static final String COLUMN_ID = "id";
    public static final String COLUMN_TITLE = "title";
    public static final String COLUMN_COST = "cost";
    public static final String COLUMN_OWNER_ID = "owner_id";


    @DatabaseField(columnName = COLUMN_ID, generatedId = true)
    private int id;

    @DatabaseField(columnName = COLUMN_TITLE)
    private String title;

    @DatabaseField(columnName = COLUMN_COST)
    private Float cost;

    @DatabaseField(columnName = COLUMN_OWNER_ID)
    private int owner_id;

    public Dish() {
    }

    public String getTitle() {
        return title;
    }

    public Dish setTitle(String title) {
        this.title = title;
        return this;
    }

    public int getId() {
        return id;
    }

    public Dish setId(int id) {
        this.id = id;
        return this;
    }

    public Float getCost() {
        return cost;
    }

    public Dish setCost(Float cost) {
        this.cost = cost;
        return this;
    }

    public int getOwnerId() {
        return owner_id;
    }

    public Dish setName(int owner_id) {
        this.owner_id = owner_id;
        return this;
    }

    public static Dish findById(List<Dish> list,long id)
    {
        for (Dish d : list) {
            if (d.getId() == id) return d;
        }
        return null;
    }


    public class Response {
        private List<Dish> success;
        private List<Error> errors;

        public List<Error> getErrors() {
            return errors;
        }

        public List<Dish> getDishs() {
            return success;
        }
    }
}