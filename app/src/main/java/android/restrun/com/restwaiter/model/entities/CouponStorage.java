package android.restrun.com.restwaiter.model.entities;

import com.j256.ormlite.field.DatabaseField;

public class CouponStorage
{
    public static final String TABLE_NAME = "couponstorage";
    public static final String COLUMN_ID = "id";
    public static final String COLUMN_USER_ID = "user_id";
    public static final String COLUMN_CARD_ID = "card_id";

    @DatabaseField(columnName = COLUMN_ID, generatedId = true)
    private long id;

    @DatabaseField(columnName = COLUMN_USER_ID)
    private long user_id;

    @DatabaseField(columnName = COLUMN_CARD_ID)
    private long card_id;

    public CouponStorage() {}

    public void setUserId(long user_id)
    {
        this.user_id = user_id;
    }

    public void setCardId(long card_id)
    {
        this.card_id = card_id;
    }
}