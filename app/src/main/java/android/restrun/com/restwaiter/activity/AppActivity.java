package android.restrun.com.restwaiter.activity;

import android.app.AlarmManager;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.SystemClock;
import android.restrun.com.restwaiter.App;
import android.restrun.com.restwaiter.R;
import android.restrun.com.restwaiter.domain.AuthDomain;
import android.restrun.com.restwaiter.fragment.OrderFragment;
import android.restrun.com.restwaiter.helpers.CurrentClient;
import android.restrun.com.restwaiter.helpers.CurrentRestaurant;
import android.restrun.com.restwaiter.helpers.CurrentUser;
import android.restrun.com.restwaiter.helpers.SimpleLogger;
import android.restrun.com.restwaiter.model.entities.Client;
import android.restrun.com.restwaiter.model.entities.Restaurant;
import android.restrun.com.restwaiter.model.entities.User;
import android.restrun.com.restwaiter.model.helpers.LockOrientation;
import android.restrun.com.restwaiter.network.OrderReciever;
import android.restrun.com.restwaiter.repository.AuthRepository;
import android.restrun.com.restwaiter.repository.ClientRepository;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import rx.Subscriber;

import static android.restrun.com.restwaiter.Constant.BROADCAST_ACTION;
import static android.restrun.com.restwaiter.Constant.BROADCAST_ACTION_ORDER_NEW;

public class AppActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener
{
    protected Fragment fragment = null;
    private final static String FRAGMENT_MAIN = "main";
    private AuthRepository authRepo = new AuthRepository();
    private ClientRepository clientRepo = new ClientRepository();
    private ProgressBar progressBar;

    NotificationManager nm;
    AlarmManager am;
    Intent intent;
    PendingIntent pIntent;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        nm = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        am = (AlarmManager) getSystemService(ALARM_SERVICE);
        new LockOrientation(this).lock();
        this.fetchUser(new AuthDomain(this).getLocalUserId());
    }

    private void fetchUser(long userId)
    {
        this.startProgress();
        authRepo.getUser(new Subscriber<User.Response>() {
            @Override
            public void onCompleted() {}
            @Override
            public void onError(Throwable e) {}
            @Override
            public void onNext(User.Response response)
            {
                if (response.getUsers().size() > 0) {
                    App.inst().user().set(response.getUser());
                    fetchClient(response.getUsers().get(0).getClientId());
                } else {
                    Toast.makeText(AppActivity.this, "User is not found", Toast.LENGTH_SHORT).show();
                    Log.e("API Error: ", "User.Response response is empty!");
                }

            }
        }, userId);
    }

    private void fetchClient(long clientId)
    {
        clientRepo.getClient(new Subscriber<Client.Response>() {
            @Override
            public void onCompleted() {}
            @Override
            public void onError(Throwable e)
            {
                SimpleLogger.log(AppActivity.this, e.getLocalizedMessage());
            }
            @Override
            public void onNext(Client.Response response)
            {
                if (response.getClients().size() > 0) {
                    App.inst().client().set(response.getClient());
                    setViews();
                    fetchRestaurant();
                } else {
                    Toast.makeText(AppActivity.this, "Profile is not found", Toast.LENGTH_SHORT).show();
                    Log.e("API Error: ", "Client.Response response is empty!");
                }
            }
        }, clientId);
    }

    private void fetchRestaurant()
    {
        clientRepo.getRestaurantByClient(new Subscriber<Restaurant.Response>() {
            @Override
            public void onCompleted() {}
            @Override
            public void onError(Throwable e)
            {
                SimpleLogger.log(AppActivity.this, e.getLocalizedMessage());
            }
            @Override
            public void onNext(Restaurant.Response response)
            {
                if (response.getRestaurants().size() > 0) {
                    App.inst().restaurant().set(response.getRestaurant());
                    initOrderList();
                    initService();
                } else {
                    SimpleLogger.fire(AppActivity.this, "Restaurant is not found", "Restaurant.Response response is empty!");
                }
            }
        }, App.inst().client().get().getId());
    }

    Intent createIntent(String action, String extra)
    {
        Intent intent = new Intent(this, OrderReciever.class);
        intent.setAction(action);
        intent.putExtra("extra", extra);
        return intent;
    }

    public void initService()
    {
        intent = createIntent(BROADCAST_ACTION, "TEST");
        pIntent = PendingIntent.getBroadcast(this, 0, intent, 0);
        sendBroadcast(intent);
        am.setRepeating(AlarmManager.ELAPSED_REALTIME,
                SystemClock.elapsedRealtime() + 3000, 5000, pIntent);
    }

    public void initOrderList()
    {
        this.stopProgress();
        fragment = new OrderFragment();
        this.initFt(String.valueOf(FRAGMENT_MAIN));
    }

    private void setViews()
    {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        TextView txtClientTitle = (TextView)navigationView.getHeaderView(0).findViewById(R.id.clientTitle);
        TextView txtUserTitle = (TextView)navigationView.getHeaderView(0).findViewById(R.id.userTitle);
        txtClientTitle.setText(App.inst().client().get().getTitle());
        txtUserTitle.setText(App.inst().user().get().getTitle());
    }

    @Override
    public void onBackPressed()
    {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            int count = fragment.getFragmentManager().getBackStackEntryCount();
            if (count > 1) {
                super.onBackPressed();
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_manage) {

        }
//        if (id == R.id.nav_camera) {
//            // Handle the camera action
//        } else if (id == R.id.nav_gallery) {
//
//        } else if (id == R.id.nav_slideshow) {
//
//        } else if (id == R.id.nav_manage) {
//
//        } else if (id == R.id.nav_share) {
//
//        } else if (id == R.id.nav_send) {
//
//        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void initFt(String item)
    {
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.replace(R.id.content_main, fragment);
        ft.addToBackStack(item);
        ft.commit();
    }

    private void startProgress()
    {
        progressBar.setVisibility(ProgressBar.VISIBLE);
    }

    private void stopProgress()
    {
        progressBar.setVisibility(ProgressBar.INVISIBLE);
    }
}
