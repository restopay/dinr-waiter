package android.restrun.com.restwaiter.model.helpers;

import android.content.Context;
import android.content.SharedPreferences;

public class LocalStorage
{
    SharedPreferences prefs;

    public LocalStorage(Context ctx)
    {
        prefs = ctx.getSharedPreferences("rest.run", Context.MODE_PRIVATE);
    }

    public void putLong(String name, long value)
    {
        prefs.edit().putLong(name, value).apply();
    }

    public long getLong(String name, long value)
    {
        return prefs.getLong(name, value);
    }

    public void putString(String name, String value)
    {
        prefs.edit().putString(name, value).apply();
    }

    public String getString(String name, String value)
    {
        return prefs.getString(name, value);
    }
}
