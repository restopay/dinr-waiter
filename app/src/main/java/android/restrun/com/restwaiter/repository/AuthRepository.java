package android.restrun.com.restwaiter.repository;

import android.restrun.com.restwaiter.App;
import android.restrun.com.restwaiter.model.entities.Login;
import android.restrun.com.restwaiter.model.entities.Session;
import android.restrun.com.restwaiter.model.entities.User;
import android.restrun.com.restwaiter.network.ServerCommunicator;

import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class AuthRepository
{
    private Subscription subscribe;
    private ServerCommunicator communicator;

    public AuthRepository()
    {
        communicator = App.inst().getCommunicator();
    }

    public void getLogin(Subscriber<Login.Response> cb, String login, String password)
    {
        subscribe = Observable.just(1)
                .subscribeOn(Schedulers.newThread())
                .flatMap(empty -> communicator.getLogin(login, password))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(cb);
    }

    public void createSession(Subscriber<Session.Response> cb, Session session)
    {
        subscribe = Observable.just(1)
                .subscribeOn(Schedulers.newThread())
                .flatMap(empty -> communicator.createSession(session))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(cb);
    }

    public void getUser(Subscriber<User.Response> cb, long id)
    {
        subscribe = Observable.just(1)
                .subscribeOn(Schedulers.newThread())
                .flatMap(empty -> communicator.getUser(id))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(cb);
    }
}
