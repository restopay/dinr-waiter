package android.restrun.com.restwaiter.model.helpers;

public class SystemRegistry
{
    private static String ip;

    public static String getIp()
    {
        return ip;
    }

    public static void addIp(String value)
    {
        ip = value;
    }
}
