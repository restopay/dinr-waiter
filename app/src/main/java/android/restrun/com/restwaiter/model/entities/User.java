package android.restrun.com.restwaiter.model.entities;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.List;

@DatabaseTable(tableName = User.TABLE_NAME)
public class User
{
    public static final String TABLE_NAME = "user";
    public static final String COLUMN_ID = "id";
    public static final String COLUMN_REGISTRATION_ID = "registration_id";
    public static final String COLUMN_TYPE = "type";
    public static final String COLUMN_CLIENT_ID = "client_id";
    public static final String COLUMN_TITLE = "title";
    public static final String COLUMN_ROLE = "role";

    @DatabaseField(columnName = COLUMN_ID, generatedId = true)
    private long id;

    @DatabaseField(columnName = COLUMN_REGISTRATION_ID)
    private String registration_id;

    @DatabaseField(columnName = COLUMN_TYPE)
    private String type;

    @DatabaseField(columnName = COLUMN_CLIENT_ID)
    private long client_id;

    @DatabaseField(columnName = COLUMN_TITLE)
    private String title;

    @DatabaseField(columnName = COLUMN_ROLE)
    private String role;

    public long getId()
    {
        return this.id;
    }

    public long getClientId()
    {
        return this.client_id;
    }

    public String getTitle()
    {
        return this.title;
    }

    public String getRole()
    {
        return this.role;
    }

    public User() {}

    public class Response
    {
        private List<User> success;
        private List<Error> errors;

        public List<Error> getErrors()
        {
            return errors;
        }

        public List<User> getUsers()
        {
            return success;
        }
        public User getUser()
        {
            return success.get(0);
        }
    }
}