package android.restrun.com.restwaiter.model.helpers;

import android.restrun.com.restwaiter.model.entities.Restaurant;

import java.util.List;

public class RestaurantRegistry
{
    private static List<Restaurant> restaurants;
    private static Restaurant restaurant;

    public static void setList(List<Restaurant> list)
    {
        restaurants = list;
    }

    public static List<Restaurant> getList()
    {
        return restaurants;
    }

    public static Restaurant getByLocation(double latitude, double longtitude)
    {
        for (Restaurant restaurant : getList())
        {
            if (restaurant.getLatitude() == latitude &&
                    restaurant.getLongtitude() == longtitude) {
                return restaurant;
            }
        }
        return null;
    }

    public static void setOne(Restaurant object)
    {
        restaurant = object;
    }

    public static Restaurant getOne()
    {
        return restaurant;
    }
}
