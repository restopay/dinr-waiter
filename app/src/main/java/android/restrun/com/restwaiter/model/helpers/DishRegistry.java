package android.restrun.com.restwaiter.model.helpers;


import android.restrun.com.restwaiter.model.entities.Dish;

import java.util.ArrayList;
import java.util.List;

public class DishRegistry
{
    public static List<Dish> table = new ArrayList<>();
    public static List<Dish> bill = new ArrayList<>();

    public static void setTable(List<Dish> list)
    {
        table = list;
    }

    public static void addToBill(Dish dish)
    {
        bill.add(dish);
    }

    public static void flushBill()
    {
        bill.clear();
    }

    public static List<Dish> getBill()
    {
        return bill;
    }
}
