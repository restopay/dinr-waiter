package android.restrun.com.restwaiter.adapter;

import android.content.Context;
import android.restrun.com.restwaiter.R;
import android.restrun.com.restwaiter.fragment.OrderFragment;
import android.restrun.com.restwaiter.model.entities.Order;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

public class OrderAdapter extends ArrayAdapter<Order> implements View.OnClickListener
{

    public OrderAdapter(Context context, List<Order> dishs)
    {
        super(context, R.layout.row_orders, dishs);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.row_orders, parent, false);
            convertView.setTag(new OrderAdapter.ViewHolder(convertView));
        }

        OrderAdapter.ViewHolder holder = (OrderAdapter.ViewHolder) convertView.getTag();
        Order order = getItem(position);

        if (order != null) {
            holder.txtOrderName.setText(order.getNumber());
        }
        return convertView;
    }

    @Override
    public void onClick(View v) {
//            switch (v.getId()) {
//                case R.id.btn_check:
//
//                    break;
//            }
    }

    private class ViewHolder
    {
        private final TextView txtOrderName;
//            private final Button btnCheck;
        //private final CheckBox checkDishId;

        public ViewHolder(View view) {
            txtOrderName = ((TextView) view.findViewById(R.id.txt_order_name));
//                txtDishCost = ((TextView) view.findViewById(R.id.txt_dish_cost));
//                btnCheck = ((Button) view.findViewById(R.id.btn_check));
            //checkDishId = ((CheckBox) view.findViewById(R.id.check_dish));
        }
    }
}
