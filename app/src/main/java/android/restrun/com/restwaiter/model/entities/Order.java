package android.restrun.com.restwaiter.model.entities;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

@DatabaseTable(tableName = Order.TABLE_NAME)
public class Order implements Serializable
{

    public static final String STATUS_CLOSED = "closed";
    public static final String STATUS_ACTIVE = "active";
    public static final String STATUS_DISMISSED = "dismissed";

    public static final String TABLE_NAME = "order";
    public static final String COLUMN_ID = "id";
    public static final String COLUMN_NUMBER = "number";
    public static final String COLUMN_PERSONS = "persons";
    public static final String COLUMN_TTIME = "ttime";
    public static final String COLUMN_USER_ID = "user_id";
    public static final String COLUMN_WAITER_ID = "waiter_id";
    public static final String COLUMN_RESTAURANT_ID = "restaurant_id";
    public static final String COLUMN_STATUS = "status";
    public static final String COLUMN_TABLE = "table";
    public static final String COLUMN_CCARD = "ccard";
    public static final String COLUMN_WAIT_CNT = "wait_cnt";

    @DatabaseField(columnName = COLUMN_ID)
    private long id;
    @DatabaseField(columnName = COLUMN_NUMBER)
    private String number;
    @DatabaseField(columnName = COLUMN_PERSONS)
    private int persons;
    @DatabaseField(columnName = COLUMN_TTIME)
    private String ttime;
    @DatabaseField(columnName = COLUMN_USER_ID)
    private long user_id;
    @DatabaseField(columnName = COLUMN_WAITER_ID)
    private long waiter_id;
    @DatabaseField(columnName = COLUMN_RESTAURANT_ID)
    private long restaurant_id;
    @DatabaseField(columnName = COLUMN_STATUS)
    private String status;
    @DatabaseField(columnName = COLUMN_TABLE)
    private String table;
    @DatabaseField(columnName = COLUMN_CCARD)
    private String ccard;
    @DatabaseField(columnName = COLUMN_WAIT_CNT)
    private int wait_cnt;


    public Order() {}

    public long getId() {
        return id;
    }

    public String getNumber()
    {
        return this.number;
    }

    public String getStatus()
    {
        return status;
    }

    public void setUserId(long id)
    {
        this.user_id = id;
    }

    public void setTable(String table)
    {
        this.table = table;
    }

    public String getTable()
    {
        return table;
    }

    public int getWaitCnt()
    {
        return wait_cnt;
    }

    public boolean isClosed()
    {
        return Objects.equals(this.status, STATUS_CLOSED);
    }

    public boolean isDismissed()
    {
        return Objects.equals(this.status, STATUS_DISMISSED);
    }

    public boolean isActive()
    {
        return Objects.equals(this.status, STATUS_ACTIVE);
    }

    public String getTtime() {
        return ttime;
    }

    public class Response {
        private List<Order> success;
        private List<Error> errors;

        public List<Error> getErrors() {
            return errors;
        }
        public List<Order> getOrders() {
            return success;
        }

        public Order getOrder()
        {
            return success.get(0);
        }
    }
}