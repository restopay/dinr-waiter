package android.restrun.com.restwaiter.exception;

public class ResponseException extends ApiException
{
    public ResponseException(String message) {
        super(message);
    }
}
