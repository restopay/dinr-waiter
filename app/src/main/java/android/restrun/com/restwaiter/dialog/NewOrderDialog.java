package android.restrun.com.restwaiter.dialog;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.restrun.com.restwaiter.App;
import android.restrun.com.restwaiter.R;
import android.restrun.com.restwaiter.helpers.CurrentUser;
import android.restrun.com.restwaiter.helpers.SimpleLogger;
import android.restrun.com.restwaiter.model.entities.Order;
import android.restrun.com.restwaiter.repository.OrderRepository;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;

import java.io.UnsupportedEncodingException;

import rx.Subscriber;
import rx.Subscription;


public class NewOrderDialog extends DialogFragment implements OnClickListener
{
    protected OrderRepository orderRepo = new OrderRepository();
    protected View v;
    protected Order currentOrder;
    protected Subscription orderSusbcription;
    protected static boolean shown;

    final String LOG_TAG = "myLogs";

    private ConfirmListener callback;

    public interface ConfirmListener
    {
        void onConfirmSubmit(int state);
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        callback = (ConfirmListener) getTargetFragment();
    }

    @Override
    public void show(FragmentManager manager, String tag)
    {
        if (!shown) {
            super.show(manager, tag);
        }
        shown = true;
    }

    @Override
    public void onAttach(Context context)
    {
        super.onAttach(context);
        // Verify that the host activity implements the callback interface
        try {
            // Instantiate the EditNameDialogListener so we can send events to the host
            callback = (ConfirmListener) context;
        } catch (ClassCastException e) {
            // The activity doesn't implement the interface, throw exception
            throw new ClassCastException(context.toString()
                    + " must implement ConfirmListener");
        }
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        this.v = inflater.inflate(R.layout.dialog_confirm, null);
        this.fetchArguments();
        this.setListeners();
        try {
            this.setViews();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return v;
    }

    @Override
    public void onStart() {
        super.onStart();
        this.setDialog();

    }

    private void fetchArguments()
    {
        Bundle bundle = getArguments();
        if (bundle != null) {
            this.currentOrder = (Order) bundle.getSerializable("order");
        }
    }

    @SuppressLint("SetTextI18n")
    private void setViews() throws UnsupportedEncodingException
    {
        Bundle bundle = getArguments();
        if (bundle != null) {
            TextView tv = (TextView)this.v.findViewById(R.id.table);
            tv.setText(new String("Стол #".getBytes("Windows-1251"), "UTF-8") + this.currentOrder.getTable());
        }
    }

    private void setListeners()
    {
        this.v.findViewById(R.id.btnConfirmYes).setOnClickListener(this);
        this.v.findViewById(R.id.btnConfirmNo).setOnClickListener(this);
    }

    private void setDialog()
    {
        Dialog dialog = getDialog();
        dialog.setTitle(R.string.new_order);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
    }

    public void onClick(View v)
    {
        switch (v.getId()) {
            case R.id.btnConfirmYes:
                orderSusbcription = orderRepo.activateOrder(new Subscriber<Order.Response>() {
                    @Override
                    public void onCompleted() {}
                    @Override
                    public void onError(Throwable e)
                    {
                        Log.e("API Error: ", e.getLocalizedMessage());
                    }
                    @Override
                    public void onNext(Order.Response response)
                    {
                        if (response != null && response.getOrders().size() > 0) {
                            Order order = response.getOrder();
                            if (!order.isActive()) {
                                Log.e("API Error: ", "Error of dismissing order!");
                            } else {
                                Log.i("API Success: ", "Order has activated!");
                                callback.onConfirmSubmit(1);
                            }
                        } else {
                            Log.e("API Error: ", "Order.Response response is empty!");
                        }
                    }
                }, currentOrder.getId(), App.inst().user().get().getId());

                dismiss();
                break;
            case R.id.btnConfirmNo:
                orderSusbcription = orderRepo.dismissOrder(new Subscriber<Order.Response>() {
                    @Override
                    public void onCompleted() {}
                    @Override
                    public void onError(Throwable e)
                    {
                        SimpleLogger.log(getActivity(), e.getLocalizedMessage());
                    }
                    @Override
                    public void onNext(Order.Response response)
                    {
                        if (response != null) {
                            Order order = response.getOrder();
                            if (!order.isDismissed()) {
                                SimpleLogger.log(getActivity(), "Error of dismissing order!");
                            } else {
                                SimpleLogger.success(getActivity(), "Order has dismissed!");
                            }
                        } else {
                            SimpleLogger.fire(getActivity(), "Order is not found", "Order.Response response is empty!");
                        }
                    }
                }, currentOrder.getId());
                //callback.onConfirmSubmit(0);
                dismiss();
                break;
        }
    }

    @Override
    public void onStop()
    {
        if (orderSusbcription != null && orderSusbcription.isUnsubscribed()) {
            orderSusbcription.unsubscribe();
        }
        super.onStop();
    }

    public void onDismiss(DialogInterface dialog)
    {
        super.onDismiss(dialog);
        shown = false;
    }

    public void onCancel(DialogInterface dialog)
    {
        super.onCancel(dialog);
        Log.d(LOG_TAG, "Dialog 1: onCancel");
    }

}
