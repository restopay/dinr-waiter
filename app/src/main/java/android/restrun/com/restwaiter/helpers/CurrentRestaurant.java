package android.restrun.com.restwaiter.helpers;

import android.restrun.com.restwaiter.model.entities.Client;
import android.restrun.com.restwaiter.model.entities.Restaurant;

public class CurrentRestaurant
{
    private static CurrentRestaurant instance;
    private Restaurant restaurant;

    public static CurrentRestaurant getInstance()
    {
        if (instance != null) return instance;
        instance = new CurrentRestaurant();
        return instance;
    }

    private CurrentRestaurant() {}

    public void set(Restaurant restaurant)
    {
        this.restaurant = restaurant;
    }

    public Restaurant get()
    {
        return this.restaurant;
    }

}
