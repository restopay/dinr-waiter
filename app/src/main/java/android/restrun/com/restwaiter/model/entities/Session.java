package android.restrun.com.restwaiter.model.entities;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.List;

@DatabaseTable(tableName = Session.TABLE_NAME)
public class Session
{
    public static final String TABLE_NAME = "session";
    public static final String COLUMN_ID = "id";
    public static final String COLUMN_IP = "ip";
    public static final String COLUMN_CREATED = "created";
    public static final String COLUMN_VALID_TILL = "valid_till";
    public static final String COLUMN_LOGIN_ID = "login_id";
    public static final String COLUMN_TOKEN = "token";


    @DatabaseField(columnName = COLUMN_ID, generatedId = true)
    private long id;

    @DatabaseField(columnName = COLUMN_IP)
    private String ip;

    @DatabaseField(columnName = COLUMN_CREATED)
    private String created;

    @DatabaseField(columnName = COLUMN_VALID_TILL)
    private String valid_till;

    @DatabaseField(columnName = COLUMN_LOGIN_ID)
    private long login_id;

    @DatabaseField(columnName = COLUMN_TOKEN)
    private String token;

    public Session() {}

    public long getId()
    {
        return this.id;
    }

    public void setLoginId(long loginId)
    {
        this.login_id = loginId;
    }

    public void setCreated(String created)
    {
        this.created = created;
    }

    public void setValidTill(String validTill)
    {
        this.valid_till = validTill;
    }

    public void setIp(String ip)
    {
        this.ip = ip;
    }

    public class Response
    {
        private List<Session> success;
        private List<Error> errors;

        public List<Error> getErrors()
        {
            return errors;
        }

        public List<Session> getObjects()
        {
            return success;
        }
    }
}