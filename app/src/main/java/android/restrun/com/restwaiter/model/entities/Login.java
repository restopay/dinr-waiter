package android.restrun.com.restwaiter.model.entities;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.List;

@DatabaseTable(tableName = Login.TABLE_NAME)
public class Login
{
    public static final String TABLE_NAME = "login";
    public static final String COLUMN_ID = "id";
    public static final String COLUMN_LOGIN = "login";
    public static final String COLUMN_PASSWORD = "password";
    public static final String COLUMN_ENABLED= "enabled";


    @DatabaseField(columnName = COLUMN_ID, generatedId = true)
    private long id;

    @DatabaseField(columnName = COLUMN_LOGIN)
    private String login;

    @DatabaseField(columnName = COLUMN_PASSWORD)
    private String password;

    @DatabaseField(columnName = COLUMN_ENABLED)
    private int enabled;

    public Login() {}

    public long getId()
    {
        return this.id;
    }

    public class Response
    {
        private List<Login> success;
        private List<Error> error;

        public List<Error> getErrors()
        {
            return error;
        }

        public List<Login> getLogin()
        {
            return success;
        }
    }
}