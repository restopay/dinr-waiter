package android.restrun.com.restwaiter.network;


import android.restrun.com.restwaiter.model.entities.Bill;
import android.restrun.com.restwaiter.model.entities.Card;
import android.restrun.com.restwaiter.model.entities.CardMatrix;
import android.restrun.com.restwaiter.model.entities.Client;
import android.restrun.com.restwaiter.model.entities.CouponStorage;
import android.restrun.com.restwaiter.model.entities.Dish;
import android.restrun.com.restwaiter.model.entities.Invoice;
import android.restrun.com.restwaiter.model.entities.Login;
import android.restrun.com.restwaiter.model.entities.Order;
import android.restrun.com.restwaiter.model.entities.Restaurant;
import android.restrun.com.restwaiter.model.entities.Session;
import android.restrun.com.restwaiter.model.entities.User;

import java.util.List;

import retrofit.http.Body;
import retrofit.http.DELETE;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.PUT;
import retrofit.http.Path;
import retrofit.http.Query;
import rx.Observable;

public interface IWebService
{
    @GET("/client/{id}")
    Observable<Client.Response> getClient(@Path("id") long id);

    @GET("/restaurant/")
    Observable<Restaurant.Response> getRestaurantByClient(@Query("client_id") long clientId);

    @GET("/invoice/")
    Observable<Invoice.Response> getInvoiceByOrder(@Query("order_id") long orderId);

    @GET("/bill/")
    Observable<Bill.Response> getBillByInvoice(@Query("invoice_id") long invoiceId);

    @GET("/login/")
    Observable<Login.Response> getLogin(@Query("login") String login, @Query("hash") String password);

    @GET("/user/{id}")
    Observable<User.Response> getUser(@Path("id") long id);

    @POST("/session/")
    Observable<Session.Response> createSession(@Body Session session);

    @GET("/order/todaysorders/")
    Observable<Order.Response> getTodaysOrders(@Query("user_id") long restId);

    @GET("/order/waitorders/")
    Observable<Order.Response> getTodaysWaitOrders(@Query("restaurant_id") long restId);

    @GET("/order/waitorders/")
    Observable<Order.Response> getWaitOrders(@Query("restaurant_id") long restId, @Query("user_id") long userId, @Query("stamp") String stamp);

    @PUT("/invoice/")
    Observable<Invoice.Response> updateInvoice(@Body Invoice invoice);

    @GET("/invoice/approve/")
    Observable<Invoice.Response> approveInvoice(@Query("id") long invoiceId);

    @GET("/order/close/")
    Observable<Order.Response> closeOrder(@Query("id") long order_id);

    @GET("/invoice/pay/")
    Observable<Invoice.Response> payInvoice(@Query("id") long invoiceId);

    @GET("/order/dismiss/")
    Observable<Order.Response> dismissOrder(@Query("id") long order_id);

    @GET("/order/active/")
    Observable<Order.Response> activateOrder(@Query("id") long orderId, @Query("waiter_id") long waiterId);

    @DELETE("/bill/")
    Observable<Bill.Response> deleteBill(@Query("id") long billId);
}