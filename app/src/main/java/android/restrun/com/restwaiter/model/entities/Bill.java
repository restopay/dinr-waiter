package android.restrun.com.restwaiter.model.entities;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.List;

@DatabaseTable(tableName = Bill.TABLE_NAME)
public class Bill
{
    public static final String TABLE_NAME = "card";
    public static final String COLUMN_ID = "id";
    public static final String COLUMN_DISH_ID = "dish_id";
    public static final String COLUMN_INVOICE_ID = "invoice_id";
    public static final String COLUMN_TITLE = "title";
    public static final String COLUMN_COST = "cost";
    public static final String COLUMN_QUANTITY = "quantity";

    @DatabaseField(columnName = COLUMN_ID, generatedId = true)
    private long id;

    @DatabaseField(columnName = COLUMN_DISH_ID)
    private long dish_id;

    @DatabaseField(columnName = COLUMN_INVOICE_ID)
    private long invoice_id;

    @DatabaseField(columnName = COLUMN_TITLE)
    private String title;

    @DatabaseField(columnName = COLUMN_COST)
    private double cost;

    @DatabaseField(columnName = COLUMN_QUANTITY)
    private int quantity;

    public Bill() {}

    public long getId()
    {
        return this.id;
    }

    public void setDishId(int id)
    {
        this.dish_id = id;
    }

    public void setTitle(String title)
    {
        this.title = title;
    }

    public String getTitle()
    {
        return this.title;
    }

    public int getQuantity()
    {
        return this.quantity;
    }

    public void setCost(double cost)
    {
        this.cost = cost;
    }

    public class Response {
        private List<Bill> success;
        private List<Error> errors;

        public List<Error> getErrors() {
            return errors;
        }
        public List<Bill> getAll() {
            return success;
        }
    }
}