package android.restrun.com.restwaiter;

import android.app.Application;
import android.restrun.com.restwaiter.helpers.CurrentClient;
import android.restrun.com.restwaiter.helpers.CurrentRestaurant;
import android.restrun.com.restwaiter.helpers.CurrentUser;
import android.restrun.com.restwaiter.model.DatabaseHelper;
import android.restrun.com.restwaiter.network.ServerCommunicator;
import android.restrun.com.restwaiter.utils.LogUtils;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;

public class App extends Application
{
    private static App instance;

    private DatabaseHelper database;
    private ServerCommunicator serverCommunicator;
    private CurrentUser currentUser;
    private CurrentClient currentClient;
    private CurrentRestaurant currentRestaurant;

    public static App inst()
    {
        if (instance == null) {
            throw new IllegalStateException("You're trying to access App too early");
        }
        return instance;
    }

    public CurrentUser user()
    {
        return currentUser;
    }

    public CurrentRestaurant restaurant()
    {
        return currentRestaurant;
    }

    public CurrentClient client()
    {
        return currentClient;
    }

    @Override
    public void onCreate()
    {
        super.onCreate();
        database = new DatabaseHelper(this);
        instance = this;
        currentUser = CurrentUser.getInstance();
        currentClient = CurrentClient.getInstance();
        currentRestaurant = CurrentRestaurant.getInstance();

        try {
            setServerCommunicator(Constant.SERVER_ENDPOINT);
        } catch (CertificateException
                | NoSuchAlgorithmException
                | KeyStoreException
                | KeyManagementException
                | IOException e) {
            e.printStackTrace();
        }
    }

    public void setServerCommunicator(final String url) throws CertificateException, NoSuchAlgorithmException, KeyStoreException, IOException, KeyManagementException {
        serverCommunicator = new ServerCommunicator(url, this);
    }

    @Override
    public void onTerminate()
    {
        database.close();
        super.onTerminate();
    }

    public void handleUncaughtException(Thread thread, Throwable e)
    {
        e.printStackTrace();
        LogUtils.extractLogToFile(this);
        System.exit(1);
    }

    public ServerCommunicator getCommunicator()
    {
        return serverCommunicator;
    }

    public DatabaseHelper getDatabase()
    {
        return database;
    }
}