package android.restrun.com.restwaiter.repository;

import android.restrun.com.restwaiter.App;
import android.restrun.com.restwaiter.model.entities.Bill;
import android.restrun.com.restwaiter.model.entities.Invoice;
import android.restrun.com.restwaiter.model.entities.Login;
import android.restrun.com.restwaiter.model.entities.Order;
import android.restrun.com.restwaiter.model.entities.Session;
import android.restrun.com.restwaiter.model.entities.User;
import android.restrun.com.restwaiter.network.ServerCommunicator;

import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class OrderRepository
{
    private Subscription subscribe;
    private ServerCommunicator communicator;

    public OrderRepository()
    {
        communicator = App.inst().getCommunicator();
    }

    public void getTodaysOrders(Subscriber<Order.Response> cb, long userId)
    {
        subscribe = Observable.just(1)
                .subscribeOn(Schedulers.newThread())
                .flatMap(empty -> communicator.getTodaysOrders(userId))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(cb);
    }

    public void getWaitOrders(Subscriber<Order.Response> cb, long restId, long userId, String stamp)
    {
        subscribe = Observable.just(1)
                .subscribeOn(Schedulers.newThread())
                .flatMap(empty -> communicator.getWaitOrders(restId, userId, stamp))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(cb);
    }

    public void getTodaysWaitOrders(Subscriber<Order.Response> cb, long restId)
    {
        subscribe = Observable.just(1)
                .subscribeOn(Schedulers.newThread())
                .flatMap(empty -> communicator.getTodaysWaitOrders(restId))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(cb);
    }

    public void getInvoicesByOrder(Subscriber<Invoice.Response> cb, long orderId)
    {
        subscribe = Observable.just(1)
                .subscribeOn(Schedulers.newThread())
                .flatMap(empty -> communicator.getInvoiceByOrder(orderId))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(cb);
    }

    public void getBillByInvoice(Subscriber<Bill.Response> cb, long invoiceId)
    {
        subscribe = Observable.just(1)
                .subscribeOn(Schedulers.newThread())
                .flatMap(empty -> communicator.getBillByInvoice(invoiceId))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(cb);
    }

    public void updateInvoice(Subscriber<Invoice.Response> cb, Invoice invoice)
    {
        subscribe = Observable.just(1)
                .subscribeOn(Schedulers.newThread())
                .flatMap(empty -> communicator.updateInvoice(invoice))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(cb);
    }

    public void approveInvoice(Subscriber<Invoice.Response> cb, long invoiceId)
    {
        subscribe = Observable.just(1)
                .subscribeOn(Schedulers.newThread())
                .flatMap(empty -> communicator.approveInvoice(invoiceId))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(cb);
    }

    public void closeOrder(Subscriber<Order.Response> cb, long orderId)
    {
        subscribe = Observable.just(1)
                .subscribeOn(Schedulers.newThread())
                .flatMap(empty -> communicator.closeOrder(orderId))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(cb);
    }

    public void deleteBill(Subscriber<Bill.Response> cb, long id)
    {
        subscribe = Observable.just(1)
                .subscribeOn(Schedulers.newThread())
                .flatMap(empty -> communicator.deleteBill(id))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(cb);
    }

    public Subscription dismissOrder(Subscriber<Order.Response> cb, long orderId)
    {
        return subscribe = Observable.just(1)
                .subscribeOn(Schedulers.newThread())
                .flatMap(empty -> communicator.dismissOrder(orderId))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(cb);
    }

    public Subscription activateOrder(Subscriber<Order.Response> cb, long orderId, long waiterId)
    {
        return subscribe = Observable.just(1)
                .subscribeOn(Schedulers.newThread())
                .flatMap(empty -> communicator.activateOrder(orderId, waiterId))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(cb);
    }

    public void closeInvoice(Subscriber<Invoice.Response>cb, long invoiceId)
    {
        subscribe = Observable.just(1)
                .subscribeOn(Schedulers.newThread())
                .flatMap(empty -> communicator.payInvoice(invoiceId))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(cb);
    }
}
