package android.restrun.com.restwaiter.fragment;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.os.Bundle;
import android.restrun.com.restwaiter.R;
import android.restrun.com.restwaiter.helpers.CurrentClient;
import android.restrun.com.restwaiter.helpers.SimpleLogger;
import android.restrun.com.restwaiter.model.entities.Bill;
import android.restrun.com.restwaiter.model.entities.Invoice;
import android.restrun.com.restwaiter.model.entities.Order;
import android.restrun.com.restwaiter.model.entities.Restaurant;
import android.restrun.com.restwaiter.repository.ClientRepository;
import android.restrun.com.restwaiter.repository.OrderRepository;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import rx.Subscriber;

public class GuestsFragment extends Fragment implements View.OnClickListener
{
    private ListView mLvInvoices;
    private GuestsAdapter mAdapter;
    private List<Invoice> mInvoices = new ArrayList<>();
    private OrderRepository orderRepo = new OrderRepository();
    private ClientRepository clientRepo = new ClientRepository();
    private Bundle bundle;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.content_guests, container, false);
        this.setAdapter(v);
        this.fetchData();
        this.bindEvents(v);

        return v;
    }

    @SuppressLint("NewApi")
    public void bindEvents(View v)
    {
        Button btnClose = (Button) v.findViewById(R.id.btn_close);
        btnClose.setOnClickListener(this);

        if (bundle != null) {
            if (Objects.equals(bundle.getString(Order.COLUMN_STATUS), Order.STATUS_CLOSED)) {
                btnClose.setVisibility(View.INVISIBLE);
            }
        }

    }

    public void setAdapter(View v)
    {
        mLvInvoices = (ListView) v.findViewById(R.id.lv_guests);
        mAdapter = new GuestsAdapter(getActivity(), mInvoices);
        mLvInvoices.setAdapter(mAdapter);
        mLvInvoices.setOnItemClickListener((adapterView, view, position, l) -> {
            Bundle arguments = new Bundle();
            arguments.putLong(Invoice.COLUMN_ID, mInvoices.get(position).getId());
            arguments.putString(Invoice.COLUMN_STATUS, mInvoices.get(position).getStatus());

            BillFragment gf = new BillFragment();
            gf.setArguments(arguments);

            FragmentTransaction ft = getActivity().getFragmentManager().beginTransaction();
            ft.replace(R.id.content_main, gf);
            ft.addToBackStack("guests");
            ft.commit();
        });
    }


    public void fetchData()
    {
        bundle = getArguments();
        this.fetchInvoices();
    }

    private void fetchInvoices()
    {
        if (bundle == null) {
            SimpleLogger.fire(getActivity(), "Error when getting guests!", "Bundle is null!");
            return;
        }
        orderRepo.getInvoicesByOrder(new Subscriber<Invoice.Response>() {
            @Override
            public void onCompleted() {}
            @Override
            public void onError(Throwable e)
            {
                SimpleLogger.log(getActivity(), e.getLocalizedMessage());
            }
            @Override
            public void onNext(Invoice.Response response)
            {
                if (response != null) {
                    List<Error> errors = response.getErrors();
                    List<Invoice> invoices = response.getInvoices();

                    if (errors != null && !errors.isEmpty()) {
                        String message = errors.get(0).getMessage();
                        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                    } else if (invoices != null) {
                        mInvoices.clear();
                        mInvoices.addAll(invoices);
                        mAdapter.notifyDataSetChanged();
                    }
                } else {
                    SimpleLogger.fire(getActivity(), "Invoice is not found", "Invoice.Response response is empty!");
                }
            }
        }, bundle.getLong(Order.COLUMN_ID));
    }

    @Override
    public void onClick(View v)
    {
        switch (v.getId()) {
            case R.id.btn_close:
                orderRepo.closeOrder(new Subscriber<Order.Response>() {
                    @Override
                    public void onCompleted() {}
                    @Override
                    public void onError(Throwable e)
                    {
                        SimpleLogger.log(getActivity(), e.getLocalizedMessage());
                    }
                    @Override
                    public void onNext(Order.Response response)
                    {
                        if (response != null && response.getOrders().size() > 0) {
                            Order order = response.getOrder();
                            if (!order.isClosed()) {
                                SimpleLogger.log(getActivity(), "Error of closing bill!");
                            } else {
                                SimpleLogger.success(getActivity(), "Order has closed!");

                                FragmentTransaction ft = getActivity().getFragmentManager().beginTransaction();
                                ft.replace(R.id.content_main, new OrderFragment());
                                ft.addToBackStack("order");
                                ft.commit();
                            }
                        } else {
                            SimpleLogger.fire(getActivity(), "Order is not found", "Order.Response response is empty!");
                        }
                    }
                }, bundle.getLong(Order.COLUMN_ID));

        }
    }

    private class GuestsAdapter extends ArrayAdapter<Invoice> implements View.OnClickListener {

        public GuestsAdapter(Context context, List<Invoice> dishs) {
            super(context, R.layout.row_guests, dishs);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = LayoutInflater.from(getContext()).inflate(R.layout.row_guests, parent, false);
                convertView.setTag(new ViewHolder(convertView));
            }

            ViewHolder holder = (ViewHolder) convertView.getTag();
            Invoice invoice = getItem(position);

            if (invoice != null) {
                holder.txtName.setText(invoice.getClientTitle());
                holder.txtStatus.setText(invoice.getLocalizedStatus());
            }
            return convertView;
        }

        @Override
        public void onClick(View v)
        {

        }

        private class ViewHolder
        {
            private final TextView txtName;
            private final TextView txtStatus;
//            private final Button btnCheck;
            //private final CheckBox checkDishId;

            public ViewHolder(View view) {
                txtName = ((TextView) view.findViewById(R.id.txt_name));
                txtStatus = ((TextView) view.findViewById(R.id.txt_status));
//                txtDishCost = ((TextView) view.findViewById(R.id.txt_dish_cost));
//                btnCheck = ((Button) view.findViewById(R.id.btn_check));
                //checkDishId = ((CheckBox) view.findViewById(R.id.check_dish));
            }
        }
    }
}
