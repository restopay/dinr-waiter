package android.restrun.com.restwaiter.model.entities;

import android.annotation.SuppressLint;
import android.restrun.com.restwaiter.R;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.Objects;

@DatabaseTable(tableName = Invoice.TABLE_NAME)
public class Invoice
{
    public static final String TABLE_NAME = "invoice";

    public static final String STATUS_PAID = "paid";
    public static final String STATUS_APPROVED = "approved";
    public static final String STATUS_CLOSED = "closed";
    public static final String STATUS_WAIT = "wait";
    public static final String STATUS_DRAFT = "draft";

    public static final String COLUMN_ID = "id";
    public static final String COLUMN_CLIENT_ID = "client_id";
    public static final String COLUMN_TOTAL = "total";
    public static final String COLUMN_USER_ID = "user_id";
    public static final String COLUMN_ORDER_ID = "order_id";
    public static final String COLUMN_DESCRIPTION = "description";
    public static final String COLUMN_DATE_CREATE = "date_create";
    public static final String COLUMN_DATE_UPDATE = "date_update";
    public static final String COLUMN_OPERATION = "operation";
    public static final String COLUMN_COMMENT = "comment";
    public static final String COLUMN_STATUS = "status";
    public static final String COLUMN_CLIENT_TITLE = "client_title";

    @DatabaseField(columnName = COLUMN_ID, generatedId = true)
    private long id;
    @DatabaseField(columnName = COLUMN_TOTAL)
    private double total;
    @DatabaseField(columnName = COLUMN_CLIENT_ID)
    private long client_id;
    @DatabaseField(columnName = COLUMN_USER_ID)
    private long user_id;
    @DatabaseField(columnName = COLUMN_ORDER_ID)
    private long order_id;
    @DatabaseField(columnName = COLUMN_DESCRIPTION)
    private String description;
    @DatabaseField(columnName = COLUMN_DATE_CREATE)
    private String date_create;
    @DatabaseField(columnName = COLUMN_DATE_UPDATE)
    private String date_update;
    @DatabaseField(columnName = COLUMN_OPERATION)
    private String operation;
    @DatabaseField(columnName = COLUMN_COMMENT)
    private String comment;
    @DatabaseField(columnName = COLUMN_STATUS)
    private String status;
    @DatabaseField(columnName = COLUMN_CLIENT_TITLE)
    private String client_title;

    public Invoice() {}

    public long getId()
    {
        return this.id;
    }

    public String getStatus()
    {
        return this.status;
    }

    public double getTotal()
    {
        return this.total;
    }

    public String getClientTitle()
    {
        return this.client_title;
    }

    public String getLocalizedStatus()
    {
        try {
            switch (this.status) {
                case STATUS_APPROVED:
                    return new String("Подтвержден".getBytes("Windows-1251"), "UTF-8");
                case STATUS_WAIT:
                    return new String("Ожидает".getBytes("Windows-1251"), "UTF-8");
                case STATUS_PAID:
                    return new String("Оплачен".getBytes("Windows-1251"), "UTF-8");
                case STATUS_CLOSED:
                    return new String("Закрыт".getBytes("Windows-1251"), "UTF-8");
                default:
                    return "";
            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return "";
    }

    @SuppressLint("NewApi")
    public boolean isApproved()
    {
        return Objects.equals(this.status, STATUS_APPROVED);
    }

    @SuppressLint("NewApi")
    public boolean isPaid()
    {
        return Objects.equals(this.status, STATUS_PAID);
    }

    public class Response
    {
        private List<Invoice> success;
        private List<Error> errors;

        public List<Error> getErrors() {
            return errors;
        }
        public List<Invoice> getInvoices() {
            return success;
        }

        public Invoice getInvoice()
        {
            return this.getInvoices().get(0);
        }

    }
}